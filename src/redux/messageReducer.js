import {messages} from '../data'
import {ADD_MESSAGE, DELETE_MESSAGE, SAVE_EDIT} from "./actionTypes";

const initialState = {
    messages
};

export const messageReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_MESSAGE: {
            return {...state, messages: [...state.messages, action.payload]};
        }
        case DELETE_MESSAGE:{
            let newItem = state.messages.filter(item => action.payload.messageId !== item.id)
            console.log(newItem);
            return {...state, messages: newItem};
        }
        case SAVE_EDIT: {
            const messages = state.messages.map(item => {
                if(item.id === action.payload.messageId) {
                    item.text = action.payload.messageText
                    item.editedAt = new Date();
                }
                return item;
            });
            return {
                ...state,
                messages
            }
        }
        default: return state;
    }
};