export const ADD_MESSAGE = 'ADD_MESSAGE';
export const SHOW_EDITOR = 'SHOW_EDITOR';
export const SAVE_EDIT = 'SAVE_EDIT';
export const DELETE_MESSAGE = 'DELETE_MESSAGE';