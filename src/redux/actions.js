import {ADD_MESSAGE, DELETE_MESSAGE, SAVE_EDIT, SHOW_EDITOR} from "./actionTypes";
import {userInfo} from "../data";
import { v4 as uuidv4 } from 'uuid';

export function addMessage(messageText) {
    return {
        type: ADD_MESSAGE,
        payload: {
            id: uuidv4(),
            text: messageText,
            ...userInfo,
            createdAt: Date(),
        }
    }
}

export function deleteMessage(messageId) {
    return {
        type: DELETE_MESSAGE,
        payload: {
            messageId
        }
    }
}

export function showEditor(message) {
    return {
        type: SHOW_EDITOR,
        payload: {
            message
        }
    }
}

export function saveEdit(messageId, messageText) {
    return {
        type: SAVE_EDIT,
        payload: {
            messageId,
            messageText
        }
    }
}
