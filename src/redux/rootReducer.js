import {combineReducers} from "redux";
import {messageReducer} from "./messageReducer";
import {editorReducer} from "./editorReducer";

export const rootReducer = combineReducers({
    messages: messageReducer,
    editor: editorReducer
});