import {SAVE_EDIT, SHOW_EDITOR} from "./actionTypes";

const initialState = {
    isShown: false,
    message: {
        text: ''
    }
};

export const editorReducer = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_EDITOR: {
            return {
                ...state,
                isShown: true,
                message: action.payload.message,
            };
        }
        case SAVE_EDIT: {
            return {
                ...state,
                isShown: false
            };
        }

        default: return state;
    }
};