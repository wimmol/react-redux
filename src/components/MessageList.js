import React from 'react'
import {connect} from "react-redux";
import Message from "./Message";
import {showEditor} from "../redux/actions";

class MessageList extends React.Component {
    render() {
        return (
            <div className={'message-list'}>
                {this.props.messages.map(message => {
                    return <Message
                        message={message}
                        key={message.editedAt+message.id}
                    />
                })}
            </div>
        )
    }

}

const mapDispatchToProps = {
    showEditor
};

const mapStateToProps = (state) => {
    return {
        messages: state.messages.messages
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);