import React from 'react'
import {connect} from "react-redux";

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            chatName: 'My Chat',
        };
    }
    render() {
        return (
            <div className={'header'}>
                <p className={'chat-name'}>{this.state.chatName}</p>
                <p>{this.props.messages
                    .map(user => user.userId)
                    .filter((id, index, arr) => arr.indexOf(id) === index)
                    .length} users</p>
                <p>{this.props.messages.length} messages</p>
                <div className={'last-message-date'}>
                    <p>Last message sent at {this.props.messages[this.props.messages.length - 1].createdAt}</p>
                </div>
            </div>
        )
    }

}

const mapStateToProps = (state) => {
    return {
        messages: state.messages.messages
    };
};


export default connect(mapStateToProps, null)(Header);