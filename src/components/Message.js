import React from 'react'
import {connect} from 'react-redux';
import {showEditor, deleteMessage} from "../redux/actions";

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isGearVisible: false,
            isLiked: false
        };
        this.onLike = this.onLike.bind(this);
        this.onGearE = this.onGearE.bind(this);
        this.onGearL = this.onGearL.bind(this);

        this.onEdit = this.onEdit.bind(this);
        this.onDelete = this.onDelete.bind(this);
    }
    onLike() {
        this.setState({isLiked: !this.state.isLiked})
    }

    onGearE() {
        this.setState({isGearVisible: true})
    }
    onGearL() {
        this.setState({isGearVisible: false})
    }

    onEdit() {
        console.log(this.props.message);
        this.props.showEditor(this.props.message);
    }

    onDelete() {
        console.log(this.props.message);
        this.props.deleteMessage(this.props.message.id);
    }

    render() {
        const data = this.props.message;

        return (
            <div>
                <div className={`message-wrapper${data.isYour ? ' your' : ''}`}>
                    <img className={'avatar'}  alt={'avatar'} src={data.avatar}/>
                    <div className={`message`} onPointerEnter={this.onGearE} onPointerLeave={this.onGearL}>
                        <div className={'username'}>
                            {data.user}
                            <div className={'on-wrap'}>
                                <div className={`cart ${!this.state.isGearVisible || !data.isYour ? 'display-none' : ''}`} onClick={this.onDelete}>Удалить</div>
                                <div className={`gear ${!this.state.isGearVisible ? 'display-none' : ''}`} onClick={this.onEdit} />
                            </div>
                        </div>
                        <p className={'message-text'}>{data.text}</p>
                        <p className={'message-date'}>{data.createdAt}</p>
                    </div>
                    <div className={`like ${this.state.isLiked ? ' red' : ''}${data.isYour ? 'display-none' : ''}`} onClick={this.onLike}/>
                </div>
            </div>
        )
    }

}

const mapDispatchToProps = {
    showEditor,
    deleteMessage
};

export default connect(null, mapDispatchToProps)(Message);