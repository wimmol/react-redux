import React, {Suspense} from 'react';
import Header from './Header';
import MessageList from "./MessageList";
import Input from "./Input";
import Editor from "./Editor";

class Chat extends React.Component {

    render() {
        return (
            <Suspense fallback={<h1>Loading...</h1>}>
                <div className={'chat'} >
                    <Editor />
                    <Header/>
                    <MessageList/>
                    <Input/>
                </div>
            </Suspense>
        )
    }
}

export default Chat;
