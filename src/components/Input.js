import React from 'react'
import {connect} from "react-redux";
import {addMessage, showEditor} from "../redux/actions";

class Input extends React.Component {
    constructor(props) {
        super(props);
        this.state = { text: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onArrow = this.onArrow.bind(this);
    }

    render() {
        return (
            <div className={'input'}>
                <form onSubmit={this.handleSubmit}>
                    <input
                        type={'text'}
                        className={'new-message'}
                        onChange={this.handleChange}
                        value={this.state.text}
                        onKeyDown={this.onArrow}
                    />
                    <button className={'send'}>
                        Send
                    </button>
                </form>
            </div>
        );
    }
    onArrow(e) {
        if(e.key === 'ArrowUp') {
            this.props.showEditor(this.props.messages[this.props.messages.length - 1]);
        }
    }

    handleChange(e) {
        this.setState({ text: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.text.length) {
            return;
        }
        this.props.addMessage(this.state.text);
        this.setState({text: ''});
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages.messages
    };
};


const mapDispatchToProps = {
    addMessage,
    showEditor
};

export default connect(mapStateToProps, mapDispatchToProps)(Input);