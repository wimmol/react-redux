import React from 'react'
import {connect} from "react-redux";
import {saveEdit} from "../redux/actions";

class Editor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            setText: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        return this.props.isShown ? this.getEditor() : null;
    }

    getEditor() {
        return (
            <div className={'edit-bg'}>
                <p>Edit Message</p>
                <form onSubmit={this.handleSubmit}>
                    <input
                        type={'text'}
                        className={'edit-input'}
                        onChange={this.handleChange}
                        value={this.state.text}
                    />
                    <button className={'save-button'}>
                        Save
                    </button>
                </form>
            </div>
        );
    }

    componentWillReceiveProps(props) {
        this.setState({text: props.message.text})
    }

    handleChange(e) {
        this.setState({ text: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.text.length) {
            return;
        }
        this.props.saveEdit(this.props.message.id, this.state.text);
        this.setState({text: ''});
    }
}

const mapDispatchToProps = {
    saveEdit
};

const mapStateToProps = (state) => {
    return {
        isShown: state.editor.isShown,
        message: state.editor.message,
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(Editor);