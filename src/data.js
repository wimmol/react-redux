import { v4 as uuidv4 } from 'uuid';
let messages = [];
let xhr = new XMLHttpRequest();
xhr.open('GET', 'https://edikdolynskyi.github.io/react_sources/messages.json', false);
xhr.send();
if (xhr.status === 200) {
    messages = JSON.parse(xhr.responseText);
}

messages.forEach(item => {item.editedAt = uuidv4()});

const userInfo = {
    userId: 0,
    user: 'Rick',
    avatar: 'https://raskrasil.com/wp-content/uploads/Raskrasil-Rick-and-Morty-51.jpg',
    editedAt: '',
    isYour: true
};


export {messages, userInfo};